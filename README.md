# README #

### What is this repository for? ###

* ServiceBox Mobile App Hybrid

### How do I get set up? ###

* Run the Servicebox Laravel Server on port 8000
* Download the repository
* Type ionic serve -p 5000 while in the sbmobile directory and you will be taken to a version of the app running in your browser
* Navigate to localhost:5000/ionic-lab to view android and ios versions side by side

### Contribution guidelines ###

* Fork

### Who do I talk to? ###

* edouard@stanford.edu
