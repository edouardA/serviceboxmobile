'use strict';
ServiceBoxApp.controller('TouristDetailsController', ['$scope', '$http', '$stateParams', '$q', '$ionicModal', 'CacheFactory',
	function($scope, $http, $stateParams, $q, $ionicModal, CacheFactory){
		$scope.attractionDetails = {};
		$scope.currentRoute = "";

		var attractionId = $stateParams.attractionId;

		var attractionDetailsCache = CacheFactory.get('touristDetailsCache');

		var getDetailsUrl = "http://" + domain + "/api/tourism/attractions/" + attractionId + "/details";
		var getRoutesUrl = "http://" + domain + "/api/tourism/attractions/" + attractionId + "/routes";

		if (attractionDetailsCache.get(attractionId)) {
			$scope.error = "";
			$scope.attractionDetails = attractionDetailsCache.get(attractionId)[0];
			$scope.attractionDetails.routes = attractionDetailsCache.get(attractionId)[1];
		} else {
			$q.all([$http.get(getDetailsUrl), $http.get(getRoutesUrl)]).then(function(results){
				$scope.error = "";
				$scope.attractionDetails = results[0].data.data;
				$scope.attractionDetails.routes = results[1].data.data;
				attractionDetailsCache.put(attractionId, [$scope.attractionDetails, $scope.attractionDetails.routes]);
			}, function(results){
				$scope.main.showError();
				$state.go('touristAttractions');
				$scope.error = "Could not get detailed attraction data; no internet access or problem with the database";
			});
		}

		$ionicModal.fromTemplateUrl('components/touristAttractions/details/touristAttractions-routes-template.html', {
			scope: $scope,
			animation: 'slide-in-right'
		}).then(function(modal) {
			$scope.dRoutesModal = modal;
		});

		$scope.openRoute = function(route) {
			$scope.currentRoute = route;
			$scope.dRoutesModal.show();
		};

		$scope.$on('$destroy', function() {
			$scope.dRoutesModal.remove();
		});

		if (typeof window.ga !== "undefined") window.ga.trackView('mobile-touristDetails');
}]);