'use strict';
ServiceBoxApp.controller('TouristAttractionsController', ['$scope', 'resultsCounterFactory', '$http','$ionicModal', '$state', '$ionicSideMenuDelegate', '$ionicScrollDelegate', 'circleLocationFactory', 'CacheFactory', '$q', '$ionicActionSheet', '$timeout',
	function($scope, resultsCounterFactory, $http, $ionicModal, $state, $ionicSideMenuDelegate, $ionicScrollDelegate, circleLocationFactory, CacheFactory, $q, $ionicActionSheet, $timeout) {
	$scope.touristAttractions.loaded = false;
	$scope.touristAttractionsArray = [];
	$scope.touristAttractions.categoriesArray = [];
	$scope.touristAttractions.routesArray = [{
		id : 1,
		show : false,
		name : "Highlands Ski Route"
	}, {
		id : 2,
		show : false,
		name : "Lowlands Self Drive Route"
	}, {
		id : 3,
		show : false,
		name : "Highlands Circular Route"
	}, {
		id : 4,
		show : false,
		name : "Mountain Biking/4x4 Route"
	}, {
		id : 5,
		show : false,
		name : "Central Pony Trek/ Hiking Route"
	}, {
		id : 6,
		show : false,
		name : "Southern Rustic Route"
	}, {
		id : 7,
		show : false,
		name : "King Moshoeshoe Historical Route"
	}, {
		id : 8,
		show : false,
		name : "Quthing Valley 4x4 Route"
	}];

	$scope.touristAttractions.allRoutesInUse = false;

	var getCategoriesUrl = "http://" + domain + "/api/tourism/attractions/categories";

	if (!CacheFactory.get('touristCategoriesCache')) {
		CacheFactory('touristCategoriesCache', {
			maxAge : 604800000,
			deleteOnExpire : 'aggressive',
			storageMode: 'localStorage',
			onExpire: function(key, value) {
				loadArray().then(function(){
				}, function(){
					CacheFactory.get('touristCategoriesCache').put(key, value);
				});
			}
		});
	}

	var touristCategoriesCache = CacheFactory.get('touristCategoriesCache');

	var loadCategories = function() {
		var deferred = $q.defer();
		if (touristCategoriesCache.get(getCategoriesUrl)) {
			$scope.error = "";
			$scope.touristAttractions.categoriesArray = touristCategoriesCache.get(getCategoriesUrl);
			touristCategoriesCache.put(getCategoriesUrl, $scope.touristAttractions.categoriesArray);
			deferred.resolve();
		} else {
			$http.get(getCategoriesUrl).then(function(response){
				$scope.error = "";
				$scope.touristAttractions.categoriesArray = response.data.data;
				touristCategoriesCache.put(getCategoriesUrl, $scope.touristAttractions.categoriesArray);
				deferred.resolve();
			}, function(response) {
				$scope.error = "Could not get tourism categories; no internet access or problem with the database";
				deferred.reject();
			});
		}
		return deferred.promise;
	};

	loadCategories().then(function() {}, function() {
		$scope.main.showError();
	});

	$scope.touristAttractions.filters = [function(attraction){
		var categoryid = attraction.category.data.id;
		for (var i = 0; i < $scope.touristAttractions.categoriesArray.length; i++) {
			if (categoryid === $scope.touristAttractions.categoriesArray[i].id) return $scope.touristAttractions.categoriesArray[i].selected;
		}
	}, function(attraction) {
		return $scope.main.districtsInUse[attraction.district.data.id];
	}];

	$scope.touristAttractions.filters.push(function(attraction) {
		return $scope.main.districtsInUse[attraction.district.data.id];
	});

	/* Applies filters*/
	$scope.touristAttractions.applyFilters = function(attraction) {
		for (var i = 0; i < $scope.touristAttractions.filters.length; i++) {
			if (!$scope.touristAttractions.filters[i](attraction)) {
				return false;
			}
		}
		return true;
	};

	var blankFunction = function() {};
	$scope.touristAttractions.locationInUse = false;
	$scope.touristAttractions.circle = {};
	$scope.touristAttractions.distance = 15000;
	$scope.touristAttractions.updateCircle = blankFunction;
	$scope.touristAttractions.drawCircle = blankFunction;
	$scope.touristAttractions.removeCircle = blankFunction;

	var getLocationRadius = circleLocationFactory($scope.touristAttractions, function(newFilter){
		$scope.touristAttractions.drawCircle(); //implemented in map controller
		appendDistance();
		$scope.main.doneLoading();
	}, function() {
		$scope.error = "Location not available";
		$scope.touristAttractions.locationInUse = false;
		$scope.main.doneLoading();
		$scope.main.showError();
	});

	var appendDistance = function() {
		for (var i = 0; i < $scope.touristAttractionsArray.length; i++) {
			if ($scope.touristAttractionsArray[i].distance !== undefined) return;
			var p = new google.maps.LatLng($scope.touristAttractionsArray[i].lat, $scope.touristAttractionsArray[i].lng);
			$scope.touristAttractionsArray[i].distance = Math.round((google.maps.geometry.spherical.computeDistanceBetween(p, $scope.touristAttractions.circle.center) | 0) / 1000 * 100)/100;
		}
		$scope.touristAttractionsArray.sort(function(a, b){
			return a.distance - b.distance;
		});
		console.log($scope.touristAttractionsArray);
	};

	/* Function to be run when location is toggled*/
	$scope.$watch('touristAttractions.locationInUse', function(flag, old){
		if (flag) {
			$scope.main.showLoading();
			getLocationRadius();
		} else {
			if ($scope.touristAttractions.filters.length > 1) {
				$scope.touristAttractions.filters.pop();
			}
			$scope.touristAttractions.removeCircle(); //implemented in map controller
		}
	});

	/* Function to be run whenever the distance slider is changed*/
	$scope.touristAttractions.distanceUpdated = function(distance){
		$timeout(function(){
			$scope.touristAttractions.circle.setRadius(parseInt(distance));
			$scope.touristAttractions.updateCircle();
		});
	};

	$scope.touristAttractions.getCurrentRoutes = function(flag) {
		var results = [];
		for (var i = 0; i < $scope.touristAttractions.routesArray.length; i++) {
			if ($scope.touristAttractions.routesArray[i].show) results.push($scope.touristAttractions.routesArray[i].id);
		}
		if (flag !== undefined) return results;
		if (results.length===0) return "";
		var retstr = "&route=";
		for (i = 0; i < results.length; i++) {
			retstr += results[i];
			retstr += ",";
		}
		return retstr.slice(0, -1);
	};

	if (!CacheFactory.get('touristCache')) {
		CacheFactory('touristCache', {
			maxAge : 86400000,
			deleteOnExpire : 'aggressive',
			storageMode: 'localStorage',
			onExpire: function(key, value) {
				loadArray().then(function(){
				}, function(){
					CacheFactory.get('touristCache').put(key, value);
				});
			}
		});
	}

	if (!CacheFactory.get('touristDetailsCache')) {
		CacheFactory('touristDetailsCache', {
			maxAge : 1800000,
			deleteOnExpire : 'aggressive',
			storageMode: 'localStorage'
		});
	}

	var touristCache = CacheFactory.get('touristCache');

	var loadArray = function(mapFunc) {
		var deferred = $q.defer();
		var districts = "district=1,2,3,4,5,6,7,8,9,10";
		var routes = $scope.touristAttractions.getCurrentRoutes();
		var getAttractionsUrl = "http://" + domain + "/api/tourism/attractions?" + districts + "&" + "category=1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19" + routes;
		if (touristCache.get(getAttractionsUrl)) {
			$scope.error = "";
			$scope.touristAttractionsArray = touristCache.get(getAttractionsUrl);
			$scope.touristAttractions.loaded = true;
			$scope.touristAttractions.totalResults = resultsCounterFactory($scope.touristAttractionsArray, $scope.touristAttractions.filters);
			if (mapFunc !== undefined) mapFunc();
			deferred.resolve();
		} else {
			$http.get(getAttractionsUrl).then(function(response){
				$scope.error = "";
				$scope.touristAttractionsArray = response.data.data;
				touristCache.put(getAttractionsUrl, $scope.touristAttractionsArray);
				$scope.touristAttractions.loaded = true;
				$scope.touristAttractions.totalResults = resultsCounterFactory($scope.touristAttractionsArray, $scope.touristAttractions.filters);
				if (mapFunc !== undefined) mapFunc();
				deferred.resolve();
			}, function(response) {
				$scope.error = "Could not get tourist attractions list; no internet access or problem with database";
				$scope.touristAttractions.loaded = true;
				deferred.reject();
			});
		}
		return deferred.promise;
	};

	loadArray().then(function(){
	}, function() {
		$scope.main.showError();
	});

	$scope.touristAttractions.loadArray = loadArray;

	$scope.touristAttractions.openDetailView = function(id) {
		$state.go('touristDetails', {attractionId : id});
	};

	$scope.touristAttractions.open360View = function(id) {
		$state.go('tourist360', {attractionId : id});
	};

	$scope.touristAttractions.openPhotoGalleryView = function(id) {
		$state.go('touristGallery', {attractionId : id});
	};

	$ionicModal.fromTemplateUrl('components/touristAttractions/touristAttractions-routes-modal-template.html', {
		scope: $scope,
		animation: 'slide-in-right'
	}).then(function(modal) {
		$scope.touristAttractions.routesModal = modal;
	});

	$scope.touristAttractions.updateRToggle = function() {
		var count = 0;
		for (var i = 0; i < $scope.touristAttractions.routesArray.length; i++) {
			if ($scope.touristAttractions.routesArray[i].show) count++;
		}
		if (count === $scope.touristAttractions.routesArray.length) $scope.touristAttractions.allRoutesInUse = true;
		else $scope.touristAttractions.allRoutesInUse = false;
	};

	$scope.$watch('touristAttractions.allRoutesInUse', function() {
		for (var i = 0; i < $scope.touristAttractions.routesArray.length; i++) {
			if ($scope.touristAttractions.allRoutesInUse) {
				$scope.touristAttractions.routesArray[i].show = true;
			} 
		}
	});

	$scope.touristAttractions.updateRList = function() {
		if ($scope.touristAttractions.allRoutesInUse) return;
		for (var i = 0; i < $scope.touristAttractions.routesArray.length; i++) {
			$scope.touristAttractions.routesArray[i].show = false;
		}
	};

	$scope.touristAttractions.allCategoriesInUse = true;

	$ionicModal.fromTemplateUrl('components/touristAttractions/touristAttractions-categoriesFilter-modal-template.html', {
		scope: $scope,
		animation: 'slide-in-right'
	}).then(function(modal) {
		$scope.touristAttractions.categoriesModal = modal;
	});

	$scope.$watch('touristAttractions.allCategoriesInUse', function() {
		for (var i = 0; i < $scope.touristAttractions.categoriesArray.length; i++) {
			if ($scope.touristAttractions.allCategoriesInUse) {
				$scope.touristAttractions.categoriesArray[i].selected = true;
			}
		}
	});

	$scope.touristAttractions.updateCToggle = function() {
		var count = 0;
		for (var i = 0; i < $scope.touristAttractions.categoriesArray.length; i++) {
			if ($scope.touristAttractions.categoriesArray[i].selected) count++;
		}
		if (count === $scope.touristAttractions.categoriesArray.length) $scope.touristAttractions.allCategoriesInUse = true;
		else $scope.touristAttractions.allCategoriesInUse = false;
	};

	$scope.touristAttractions.updateCList = function() {
		if ($scope.touristAttractions.allCategoriesInUse) return;
		for (var i = 0; i < $scope.touristAttractions.categoriesArray.length; i++) {
			$scope.touristAttractions.categoriesArray[i].selected = false;
		}
	};

	$scope.touristAttractions.toggleFilter = function() {
		$ionicActionSheet.show({
			cssClass : "custom-sheet",
			buttons : [{text : "Around Me"}, {text : "Category"}, {text : "District"}, {text : "Route"}],
			cancelText : "Cancel",
			titleText : "Choose Filters",
			buttonClicked : function(index) {
				switch (index) {
					case 1:
						$scope.touristAttractions.categoriesModal.show();
						break;
					case 2:
						$ionicSideMenuDelegate.toggleRight();
						break;
					case 3:
						$scope.touristAttractions.routesModal.show();
						break;
					case 0:
						$scope.touristAttractions.locationInUse = !$scope.touristAttractions.locationInUse;
						break;
					default:
						return false;
				}
				$ionicScrollDelegate.scrollTop();
				return true;
			}
		});
	};

	$scope.$on('$destroy', function() {
		$scope.touristAttractions.categoriesModal.remove();
		$scope.touristAttractions.routesModal.remove();
	});

	$scope.touristAttractions.tilesSelected = function() {
		$scope.touristAttractions.updateMapRoutes = function() {
			loadArray();
		};
		$scope.touristAttractions.updateMarkers = blankFunction;
		$scope.touristAttractions.updateCircle = blankFunction;
		$scope.touristAttractions.drawCircle = blankFunction;
		$scope.touristAttractions.removeCircle = blankFunction;
	};

	if (typeof window.ga !== "undefined") window.ga.trackView('mobile-touristAttractions');
}]);