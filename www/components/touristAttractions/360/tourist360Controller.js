'use strict';
ServiceBoxApp.controller('Tourist360Controller', ['$scope', '$http', '$stateParams', '$sce',
	function($scope, $http, $stateParams, $sce){
		$scope.trustSrc = function(src) {
			return $sce.trustAsResourceUrl(src);
		};
		$scope.views = [];
		var attractionId = $stateParams.attractionId;
		var getViewsUrl = "http://" + domain + "/api/tourism/attractions/" + attractionId + "/views";
		$http.get(getViewsUrl).then(function(response){
			$scope.error = "";
			$scope.views = response.data.data;
			for (var i = 0; i < $scope.views.length; i++) {
				var uriRE = new RegExp("'.*?'");
				$scope.views[i].uri = $scope.trustSrc($scope.views[i].view_code.match(uriRE)[0].slice(1, -1));
			}
		}, function(response){
			$scope.error = "Could not get attraction view data; no internet access or problem with the database";
		});
}]);