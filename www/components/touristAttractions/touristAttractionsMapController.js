ServiceBoxApp.controller('TouristAttractionsMapController', ['$scope', 'populateMarkersFactory', 'updateMarkersFactory', '$compile', '$http', '$q', function($scope, populateMarkersFactory, updateMarkersFactory, $compile, $http, $q){
	var routes = {
		1 : ["highlandsSkiRoute.json"],
		2 : ["lowlandsSelfDrive.json"],
		3 : ["highlandsCircularRoute.json"],
		4 : ["mountainBikingRoute.json","mountainBikingRouteP1.json", "mountainBikingRouteP2.json"],
		5 : ["centralPonyTrekRoute.json", "centralPonyTrekRouteP1.json"],
		6 : ["southernRusticRoute.json"],
		7 : ["kingMoshoeshoeHistoricalRoute.json"],
		8 : ["quthingP1Route.json", "quthingP2Route.json", "quthingP3Route.json"]
	};

	var drawRoutes = function() {
		var rs = $scope.touristAttractions.getCurrentRoutes(true);
		var toDrawArray = [];
		for (var i = 0; i < rs.length; i++) {
			for (var j = 0; j < routes[rs[i]].length; j++) {
				toDrawArray.push(routes[rs[i]][j]);
			}
		}
		var promiseArray = [];
		for (i = 0; i < toDrawArray.length; i++) {
			//if in browser
			var promise = $http.get("data/routes/"+toDrawArray[i]);
			//if emulating
			//var promise = $http.get("http://10.0.2.2:5000/data/routes/"+toDrawArray[i]);
			promiseArray.push(promise);
		}
		$q.all(promiseArray).then(function(results){
			$scope.error = "";
			for (i = 0; i < results.length; i++) {
				var path = [];
				var ret = results[i].data;
				for (var j = 0; j < ret.latlngs.length; j++) {
					var p = new google.maps.LatLng(ret.latlngs[j].lat, ret.latlngs[j].lng);
					path.push(p);
				}
				var poly = new google.maps.Polyline({
					strokeColor: ret.color,
					strokeOpcaity: 1.0,
					strokeWeight: ret.weight,
					path: path
				});
				poly.setMap($scope.touristAttractions.map);
			}
		}, function(results){
			$scope.error = "Could not get route drawing data; no internet access or problem with the database";
		});
	};

	var markers = [];

	var createMarkerClickHandler = function(attraction) {
		var content = '<div class="info-card"><div>'+attraction.name+'</div><div><a href="tel:'+attraction.phone.split(' ').join('')+'">'+attraction.phone+'</a></div><div><a href="#/directions/'+attraction.lat+'/'+attraction.lng+'">Get Directions</a></div><div><a href="#/touristDetails/'+attraction.id+'""> Details </a></div></div>';
		var compiledContent = $compile(content)($scope);
		return function($event) {
			var infoWindow = new google.maps.InfoWindow({
				content: compiledContent[0]
			});
			infoWindow.open($scope.touristAttractions.map, this);
		};
	};

	/* Function for showing the circle, map is where the circle will be shown, the update flag is whether or not to update markers*/
	var showCircle = function(map, update) {
		$scope.touristAttractions.circle.setMap(map);
		$scope.touristAttractions.updateCircle = $scope.touristAttractions.updateMarkers;
		if (update) $scope.touristAttractions.updateMarkers();
	};

	/* Set the circle functions defined in the tile view*/
	var setCircleFunctions = function() {
		$scope.touristAttractions.drawCircle = function() {
			showCircle($scope.touristAttractions.map, true);
			$scope.touristAttractions.map.setCenter($scope.touristAttractions.circle.center);
		};

		$scope.touristAttractions.removeCircle = function() {
			showCircle(null, true);
			$scope.touristAttractions.map.setCenter($scope.main.defaultMapOptions.center);
		};
	};

	$scope.touristAttractions.loadMap = function() {
		$scope.main.showLoading();
		var routes = $scope.touristAttractions.getCurrentRoutes();
		$scope.touristAttractions.map = new google.maps.Map(document.getElementById("mapta"), $scope.main.defaultMapOptions);
		markers = [];
		var populateMarkersArray = populateMarkersFactory($scope.touristAttractions.map, markers, $scope.touristAttractionsArray, function(attraction){
			return "img/tourism/" + attraction.category.data.icon;
		}, createMarkerClickHandler);
		populateMarkersArray();
		$scope.touristAttractions.updateMarkers = updateMarkersFactory($scope.touristAttractionsArray, $scope.touristAttractions.filters, markers, $scope.touristAttractions.map);
		$scope.main.districtsChanged = $scope.touristAttractions.updateMarkers;
		if (routes.length > 0) drawRoutes();
		$scope.main.doneLoading();
	};
	$scope.touristAttractions.loadMap();
	$scope.touristAttractions.updateMarkers();
	if ($scope.touristAttractions.locationInUse) showCircle($scope.touristAttractions.map, false);
	setCircleFunctions();


	var updatemr = function() {
		$scope.touristAttractions.loadArray($scope.touristAttractions.loadMap);
	};

	$scope.touristAttractions.updateMapRoutes = updatemr;

	$scope.touristAttractions.mapSelected = function() {
		$scope.main.showLoading();
		$scope.touristAttractions.updateMarkers = function() {
			var func = updateMarkersFactory($scope.touristAttractionsArray, $scope.touristAttractions.filters, markers, $scope.touristAttractions.map);
			func();
		};
		$scope.main.districtsChanged = $scope.touristAttractions.updateMarkers;
		$scope.touristAttractions.updateMarkers();
		$scope.touristAttractions.updateMapRoutes = updatemr;
		if ($scope.touristAttractions.getCurrentRoutes().length > 0) $scope.touristAttractions.updateMapRoutes();
		setTimeout(function(){
			google.maps.event.trigger($scope.touristAttractions.map, 'resize');
		}, 100);
		if ($scope.touristAttractions.locationInUse) {
			showCircle($scope.touristAttractions.map, false);
		} else if ($scope.touristAttractions.circle.setMap !== undefined) {
			showCircle(null, false);
		}
		setCircleFunctions();
		$scope.main.doneLoading();
	};
}]);