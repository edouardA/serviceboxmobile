'use strict';

ServiceBoxApp.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    };
});

ServiceBoxApp.controller('TouristGalleryController', ['$scope', '$http', '$stateParams', '$sce',
	function($scope, $http, $stateParams, $sce){
		$scope.trustSrc = function(src) {
			return $sce.trustAsResourceUrl(src);
		};
		$scope.gallery = [];
		var attractionId = $stateParams.attractionId;

		var getGalleryUrl = "http://" + domain + "/api/tourism/attractions/" + attractionId + "/photo-gallery";

		$http.get(getGalleryUrl).then(function(response){
			$scope.error = "";
			$scope.gallery = response.data.data;
			$scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
				console.log("Called");
				for (var i = 0; i < $scope.gallery.length; i++) {
					var id = "#album" + i;
					$(id).FacebookAlbumBrowser({
						account: $scope.gallery[i].fbAccountName,
                        accessToken: "1618596101799847|5pbrRihe5ZLUOaGVT8Lb20tXanA",
                        onlyAlbum: $scope.gallery[i].albumName,
                        thumbnailSize: 100,
                        showComments: false,
                        showAccountInfo: true,
                        showImageCount: true,
                        showImageText: true,
                        albumsPageSize: 4,
                        photosPageSize: 4,
                        lightbox: true,
                        photosCheckbox: false,
                        pluginImagesPath: "/img/",
                        likeButton: true,
                        shareButton: true,
                        addThis: "ra-52638e915dd79612"
					});
				}
			});
		}, function(response){
			$scope.error = "Could not get attraction phot gallery data; no internet access or problem with the database";
		});
}]);