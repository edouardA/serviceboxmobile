'use strict';
ServiceBoxApp.controller('TelephoneTaxisController', ['$scope', '$q', '$http', 'CacheFactory', '$ionicScrollDelegate', '$ionicSideMenuDelegate', '$ionicActionSheet',  function($scope, $q, $http, CacheFactory, $ionicScrollDelegate, $ionicSideMenuDelegate, $ionicActionSheet) {
	$scope.telephoneTaxis.loaded = false;
	$scope.telephoneTaxisArray = [];

	$scope.telephoneTaxis.filters = [function(telephoneTaxi){
		return $scope.main.districtsInUse[telephoneTaxi.district.data.id];
	}];

	$scope.telephoneTaxis.applyFilters = function(telephoneTaxi) {
		for (var i = 0; i < $scope.telephoneTaxis.filters.length; i++) {
			if (!$scope.telephoneTaxis.filters[i](telephoneTaxi)) {
				return false;
			}
		}
		return true;
	};
	if (!CacheFactory.get(telephoneTaxisCache)) {
		CacheFactory('telephoneTaxisCache', {
			maxAge : 604800000,
			deleteOnExpire : 'aggressive',
			storageMode: 'localStorage',
			onExpire: function(key, value) {
				loadData().then(function(){
				}, function(){
					CacheFactory.get('telephoneTaxisCache').put(key, value);
				});
			}
		});
	}

	var telephoneTaxisCache = CacheFactory.get('telephoneTaxisCache');

	var getUrl = "http://" + domain + "/api/contacts/telephone-taxis?district=1,2,3,4,5,6,7,8,9,10";

	var loadData = function() {
		var deferred = $q.defer();
		if (telephoneTaxisCache.get(getUrl)) {
			$scope.error = "";
			$scope.telephoneTaxisArray = telephoneTaxisCache.get(getUrl);
			$scope.telephoneTaxis.loaded = true;
			deferred.resolve();
		} else {
			$http.get(getUrl).then(function(response){
				$scope.error = "";
				$scope.telephoneTaxisArray = response.data.data;
				telephoneTaxisCache.put(getUrl, $scope.telephoneTaxisArray);
				$scope.telephoneTaxis.loaded = true;
				deferred.resolve();
			}, function(response){
				$scope.error = "Could not load telephone taxis information; no internet connection or problem with the database";
				deferred.reject();
			});
		}
		return deferred.promise;
	};

	loadData().then(function(){}, function(){
		$scope.main.showError();
	});
	
	$scope.telephoneTaxis.toggleFilter = function() {
		$ionicActionSheet.show({
			cssClass : "custom-sheet",
			buttons : [{text : "District"}],
			cancelText : "Cancel",
			titleText : "Choose Filters",
			buttonClicked : function(index) {
				switch (index) {
					case 0:
						$ionicSideMenuDelegate.toggleRight();
						$ionicScrollDelegate.scrollTop();
						return true;
					default:
						return false;
				}
			}
		});
	};

	if (typeof window.ga !== "undefined") window.ga.trackView('mobile-telephoneTaxis');
}]);