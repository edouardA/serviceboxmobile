'use strict';
ServiceBoxApp.controller('MobileMoneyController', ['$scope', '$http', 'resultsCounterFactory', '$ionicSideMenuDelegate', '$ionicScrollDelegate', 'CacheFactory', '$q', '$ionicActionSheet', function($scope, $http, resultsCounterFactory, $ionicSideMenuDelegate, $ionicScrollDelegate, CacheFactory, $q, $ionicActionSheet) {
	$scope.mobileMoney.agentName = "Mobile Money";
	$scope.mobileMoneyArray = [];
	$scope.mobileMoney.agentChosen = false;
	$scope.mobileMoney.searchByAreaText = "";
	$scope.mobileMoney.loaded = false;

	/* Change default district selected to maseru */
	$scope.$on('$ionicView.enter', function() {
		for (var i = 1; i <= 10; i++) {
			if (i !== 5) $scope.main.districtsInUse[i] = false;
			else $scope.main.districtsInUse[i] = true;
		}
		$scope.main.allDistrictsInUse = false;
		$scope.main.filteringByDistrict = true;
	});

	/* Load the array using the search word */
	$scope.mobileMoney.search = function(){
		loadArray($scope.mobileMoney.searchByAreaText);
	};

	/* Get the current districts as a properly formatted query string*/
	var getCurrentDistricts = function() {
		var results = [];
		for (var i = 1; i <= 10; i++) {
			if ($scope.main.districtsInUse[i]) results.push(i);
		}
		var retstr = "district=";
		for (i = 0; i < results.length; i++) {
			retstr += results[i];
			retstr += ",";
		}
		return retstr.slice(0, -1);
	};

	if (!CacheFactory.get('mobileMoneyCache')) {
		CacheFactory('mobileMoneyCache', {
			maxAge : 86400000,
			deleteOnExpire : 'aggressive',
			storageMode: 'localStorage',
			onExpire: function(key, value) {
				loadArray().then(function(){
				}, function(){
					CacheFactory.get('mobileMoneyCache').put(key, value);
				});
			}
		});
	}

	var mobileMoneyCache = CacheFactory.get('mobileMoneyCache');

	/* Load the array of agents into the controller*/
	var loadArray = function(area) {
		var deferred = $q.defer();
		$scope.mobileMoney.loaded=false;
		var districts = getCurrentDistricts();
		var getAgentsUrl = "http://" + domain + "/api/mobile-money/agents?"+districts+"&provider=" + $scope.mobileMoney.provider;
		$scope.mobileMoney.getWebsite = function () {
			if ($scope.mobileMoney.provider === "1") return "http://www.vodacom.co.ls/ls-personal/m-pesa/m-pesa";
			if ($scope.mobileMoney.provider === "2") return "http://www.etl.co.ls";
		};
		if (area !== undefined) {
			getAgentsUrl += "&search=";
			getAgentsUrl += area;
		}
		if (mobileMoneyCache.get(getAgentsUrl)) {
			$scope.error = "";
			$scope.mobileMoneyArray = mobileMoneyCache.get(getAgentsUrl);
			$scope.mobileMoney.totalAgents = resultsCounterFactory($scope.mobileMoneyArray, null);
			$scope.mobileMoney.loaded = true;
			deferred.resolve();
		} else {
			$http.get(getAgentsUrl).then(function(response){
				$scope.error = "";
				$scope.mobileMoneyArray = response.data.data;
				$scope.mobileMoney.totalAgents = resultsCounterFactory($scope.mobileMoneyArray, null);
				$scope.mobileMoney.loaded = true;
				mobileMoneyCache.put(getAgentsUrl, $scope.mobileMoneyArray);
				deferred.resolve();
			}, function(response){
				$scope.mobileMoney.loaded = true;
				$scope.error = "Could not get mobile money agents list; no internet access or problem with the database";
				deferred.reject();
			});
		}
		
		return deferred.promise;
	};

	/* Set the function to run whenever the districts are changed */
	$scope.main.districtsChanged = loadArray;

	/* Dynamically allocate the height of a card*/
	$scope.mobileMoney.getHeight = function(card) {
		if (card.hasArea) return "350px";
		return "250px";
	};

	/* Function to run whenever an agent is selected*/
	$scope.mobileMoney.agentSelected = function(id) {
		$scope.mobileMoney.agentChosen = true;
		$scope.mobileMoney.provider = id;
		if (id === "1") {
			$scope.mobileMoney.agentName = "Mpesa Agents";
		} else {
			$scope.mobileMoney.agentName = "Ecocash Agents";
		}
		loadArray().then(function(){}, function(){
			$scope.main.showError();
		});
	};

	/* Switch default to all districts in use whenever page is navigated away from */
	$scope.$on('$locationChangeStart', function(event) {
		$scope.main.allDistrictsInUse = true;
	});
	
	$scope.mobileMoney.showSearchBar = false;
	$scope.mobileMoney.toggleFilter = function() {
		$ionicActionSheet.show({
			cssClass : "custom-sheet",
			buttons : [{text : "District"}, {text : "Search by Area"}, {text : "Switch Providers"}],
			cancelText : "Cancel",
			titleText : "Choose Filters",
			buttonClicked : function(index) {
				switch (index) {
					case 0:
						$ionicSideMenuDelegate.toggleRight();
						break;
					case 2:
						if ($scope.mobileMoney.provider === "1") {
							$scope.mobileMoney.agentSelected("2");
						} else {
							$scope.mobileMoney.agentSelected("1");
						}
						break;
					case 1:
						$scope.mobileMoney.showSearchBar = !$scope.mobileMoney.showSearchBar;
						break;
					default:
						return false;
				}
				$ionicScrollDelegate.scrollTop();
				return true;
			}
		});
	};

	if (typeof window.ga !== "undefined") window.ga.trackView('mobile-mobileMoney');
}]);