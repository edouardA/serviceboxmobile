'use strict';
ServiceBoxApp.controller('PoliceController', ['$scope', '$http', 'resultsCounterFactory', '$ionicSideMenuDelegate', '$ionicModal', '$timeout', 'circleLocationFactory', '$ionicScrollDelegate', 'CacheFactory', '$q', '$ionicActionSheet',
 function($scope, $http, resultsCounterFactory, $ionicSideMenuDelegate, $ionicModal, $timeout, circleLocationFactory, $ionicScrollDelegate, CacheFactory, $q, $ionicActionSheet) {
	/*For convenience and readability*/
	var blankFunction = function(){};

	/* Defaults */
	$scope.police.loaded = false;
	$scope.police.showPosts = true;
	$scope.police.showStations = true;
	$scope.police.showHeadquarters = true;
	$scope.policeArray = [];
	$scope.police.locationInUse = false;
	$scope.police.circle = {};
	$scope.police.distance = 5000;
	$scope.police.updateCircle = blankFunction;
	$scope.police.drawCircle = blankFunction;
	$scope.police.removeCircle = blankFunction;
	
	/* Set up filters */
	$scope.police.filters = [function(policeplace){
		var typeId = policeplace.type.data.id;
		if (!$scope.police.showPosts) {
			if (typeId === 2) return false;
		}
		if (!$scope.police.showStations) {
			if (typeId === 1) return false;
		}
		if (!$scope.police.showHeadquarters) {
			if (typeId === 3) return false;
		}
		return true;
	}, function(policeplace){
		return $scope.main.districtsInUse[policeplace.district.data.id];
	}];
	var filtersPosts = $scope.police.filters.slice();
	filtersPosts.push(function(policeplace){
		return policeplace.type.data.id === 2;
	});
	var filtersStations = $scope.police.filters.slice();
	filtersStations.push(function(policeplace){
		return policeplace.type.data.id === 1;
	});
	$scope.police.extraFilters = [filtersPosts, filtersStations];

	var getLocationRadius = circleLocationFactory($scope.police, function(newFilter){
		$scope.police.extraFilters[0].push(newFilter);
		$scope.police.extraFilters[1].push(newFilter);
		$scope.police.drawCircle(); //implemented in map controller
		appendDistance();
		$scope.main.doneLoading();
	}, function(){
		$scope.error = "Location not available";
		$scope.police.locationInUse = false;
		$scope.main.doneLoading();
		$scope.main.showError();
	});

	/* Function to be run when location is toggled*/
	$scope.$watch('police.locationInUse', function(flag){
		if (flag) {
			$scope.main.showLoading();
			getLocationRadius();
		} else {
			if ($scope.police.filters.length > 2) {
				$scope.police.filters.pop();
				$scope.police.extraFilters[0].pop();
				$scope.police.extraFilters[1].pop();
			}
			$scope.police.removeCircle(); //implemented in map controller
		}
	});

	/* Function to be run whenever the distance slider is changed*/
	$scope.police.distanceUpdated = function(distance){
		$timeout(function(){
			$scope.police.circle.setRadius(parseInt(distance));
			$scope.police.updateCircle();
		});
	};

	/* Applies filters*/
	$scope.police.applyFilters = function(policeplace){
		for (var i = 0; i < $scope.police.filters.length; i++) {
			if (!$scope.police.filters[i](policeplace)) {
				return false;
			}
		}
		return true;
	};

	/*Function to create the functions which tally up responsive totals*/
	var setUpTotals = function() {
		$scope.police.totalResults = resultsCounterFactory($scope.policeArray, $scope.police.filters);
		$scope.police.totalPosts = resultsCounterFactory($scope.policeArray, filtersPosts);
		$scope.police.totalStations = resultsCounterFactory($scope.policeArray, filtersStations);
	};

	var appendDistance = function() {
		for (var i = 0; i < $scope.policeArray.length; i++) {
			var p = new google.maps.LatLng($scope.policeArray[i].latitude, $scope.policeArray[i].longitude);
			$scope.policeArray[i].distance = Math.round((google.maps.geometry.spherical.computeDistanceBetween(p, $scope.police.circle.center) | 0) / 1000 * 100) / 100;
		}
		$scope.policeArray.sort(function(a, b){
			return a.distance - b.distance;
		});
	};

	/*Logic for getting police array*/
	var getUrl = "http://" + domain + "/api/contacts/police-stations";

	if (!CacheFactory.get('policeCache')) {
		CacheFactory('policeCache', {
			maxAge : 604800000,
			deleteOnExpire : 'aggressive',
			storageMode: 'localStorage',
			onExpire: function(key, value) {
				loadData().then(function(){
				}, function(){
					CacheFactory.get('policeCache').put(key, value);
				});
			}
		});
	}

	var policeCache = CacheFactory.get('policeCache');

	var loadData = function () {
		var deferred = $q.defer();
		if (policeCache.get(getUrl)) {
			$scope.error = "";
			$scope.policeArray = policeCache.get(getUrl);
			setUpTotals();
			$scope.police.loaded = true;
			deferred.resolve();
		} else {
			$http.get(getUrl).then(function(response){
				$scope.error = "";
				$scope.policeArray = response.data.data;
				policeCache.put(getUrl, $scope.policeArray);
				setUpTotals();
				$scope.police.loaded = true;
				deferred.resolve();
			}, function(response){
				$scope.error = "Could not get police contacts; no internet access or problem with database";
				deferred.reject();
			});
		}
		return deferred.promise;
	};

	loadData().then(function() {}, function() {
		$scope.main.showError();
	});
	
	$scope.police.toggleFilter = function() {
		$ionicActionSheet.show({
			cssClass : "custom-sheet",
			buttons : [{text : 'Around Me'}, {text : 'District'}, {text : 'Type'}],
			cancelText : 'Cancel',
			titleText : 'Choose Filters',
			buttonClicked : function(index) {
				switch (index) {
					case 2:
						$scope.police.modal.show();
						break;
					case 1:
						$ionicSideMenuDelegate.toggleRight();
						break;
					case 0:
						$scope.police.locationInUse = !$scope.police.locationInUse;
						break;
					default:
						return false;
				}
				$ionicScrollDelegate.scrollTop();
				return true;
			}
		});
	};

	/*Function to reset mapping specific funcitons when in tile view*/
	$scope.police.tilesSelected = function() {
		$scope.police.updateCircle = blankFunction;
		$scope.police.drawCircle = blankFunction;
		$scope.police.removeCircle = blankFunction;
		$scope.police.updateMarkers = blankFunction;
	};

	/* Logic for Modal to display type filters */
	$ionicModal.fromTemplateUrl('components/police/police-filter-modal-template.html', {
		scope: $scope,
		animation: 'slide-in-right'
	}).then(function(modal) {
		$scope.police.modal = modal;
	});
	// Cleanup the modal when we're done with it!
	$scope.$on('$destroy', function() {
		$scope.police.modal.remove();
	});

	if (typeof window.ga !== "undefined") {
		console.log("tracked police view");
		window.ga.trackView('mobile-police');
	}
}]);

ServiceBoxApp.controller('PoliceMapController', ['$scope', 'populateMarkersFactory', 'updateMarkersFactory', function($scope, populateMarkersFactory, updateMarkersFactory){
	/* Array holding markers */
	var markers = [];

	/* Function to create the click handler for the marker icons*/
	var createMarkerClickHandler = function(policeplace) {
		var content = '<div class="info-card"><div>'+policeplace.district.data.name+'</div><div>'+policeplace.name+'</div><div> <a href="tel:'+policeplace.phone.split(' ').join('')+'">'+policeplace.phone+'</a></div>';
		content += '<div><a href="#/directions/'+policeplace.latitude+'/'+policeplace.longitude+'">Get Directions</a></div></dvi>';
		return function ($event) {
			var infoWindow = new google.maps.InfoWindow({
				content: content
			});
			infoWindow.open($scope.police.map, this);
		};
	};

	var policeMarkerIcon = function(id) {
		if (id === 1) {
			return "img/police/police-station.png";
		}
		if (id === 2) {
			return "img/police/police-post.png";
		}
		return "img/police/headquarters.png";
	};

	/* Function for showing the circle, map is where the circle will be shown, the update flag is whether or not to update markers*/
	var showCircle = function(map, update) {
		$scope.police.circle.setMap(map);
		$scope.police.updateCircle = $scope.police.updateMarkers;
		if (update) $scope.police.updateMarkers();
	};

	/* Set the circle functions defined in the tile view*/
	var setCircleFunctions = function() {
		$scope.police.drawCircle = function() {
			showCircle($scope.police.map, true);
			$scope.police.map.setCenter($scope.police.circle.center);
		};

		$scope.police.removeCircle = function() {
			showCircle(null, true);
			$scope.police.map.setCenter($scope.main.defaultMapOptions.center);
		};
	};

	/* Function to load the map into the DOM*/
	$scope.police.loadMap = function() {
		$scope.main.showLoading();
		$scope.police.map = new google.maps.Map(document.getElementById("mapp"), $scope.main.defaultMapOptions);
		var populateMarkersArray = populateMarkersFactory($scope.police.map, markers, $scope.policeArray, function(policeplace) {
			return policeMarkerIcon(policeplace.type.data.id);
		}, createMarkerClickHandler);
		populateMarkersArray();
		$scope.police.updateMarkers = updateMarkersFactory($scope.policeArray, $scope.police.filters, markers, $scope.police.map);
		$scope.main.districtsChanged = $scope.police.updateMarkers;
		$scope.main.doneLoading();
	};

	/* Function to be run whenever the map view is selected after initially loading*/
	$scope.police.mapSelected = function() {
		$scope.main.showLoading();
		$scope.police.updateMarkers = updateMarkersFactory($scope.policeArray, $scope.police.filters, markers, $scope.police.map);
		$scope.main.districtsChanged = $scope.police.updateMarkers;
		$scope.police.updateMarkers();
		setTimeout(function(){
			google.maps.event.trigger($scope.police.map, 'resize');
		}, 100);
		if ($scope.police.locationInUse) {
			showCircle($scope.police.map, false);
		} else if ($scope.police.circle.setMap !== undefined) {
			showCircle(null, false);
		}
		setCircleFunctions();
		$scope.main.doneLoading();
	};

	/* Code to be run when map view is first selected*/
	$scope.police.loadMap();
	$scope.police.updateMarkers();
	if ($scope.police.locationInUse) showCircle($scope.police.map, false);
	setCircleFunctions();
}]);