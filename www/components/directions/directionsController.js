'use strict';
ServiceBoxApp.controller("DirectionsController", ['$scope', '$stateParams', '$ionicPlatform', '$cordovaGeolocation', '$ionicHistory',
	function($scope, $stateParams, $ionicPlatform, $cordovaGeolocation, $ionicHistory){
		$scope.main.showLoading();
		var latitude = parseFloat($stateParams.lat);
		var longitude = parseFloat($stateParams.lng);
		var destination = new google.maps.LatLng(latitude, longitude);
		$scope.directions.map = new google.maps.Map(document.getElementById("mapd"), $scope.main.defaultMapOptions);
		var ds = new google.maps.DirectionsService();
		var dd = new google.maps.DirectionsRenderer();
		dd.setMap($scope.directions.map);
		dd.setPanel(document.getElementById("dp"));
		$scope.directions.back = function() {
			$ionicHistory.goBack();
			$scope.main.showMenu=true;
		};
		$ionicPlatform.ready(function(){
				$cordovaGeolocation.getCurrentPosition({timeout : 5000}).then(function(position){
					var lat = position.coords.latitude;
					var lng = position.coords.longitude;
					var origin = new google.maps.LatLng(lat, lng);
					var request = {
						origin: origin,
						destination:destination,
						travelMode: google.maps.TravelMode.DRIVING
					};
					ds.route(request, function(result, status) {
						if (status == google.maps.DirectionsStatus.OK) {
							dd.setDirections(result);
						}
					});
					$scope.main.doneLoading();
				}, function(error){
					$scope.main.doneLoading();
					$ionicHistory.goBack();
					$scope.main.showError();
			});
		});

		if (typeof window.ga !== "undefined") window.ga.trackView('mobile-directions');
}]);