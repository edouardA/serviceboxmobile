'use strict';

//For use in production
//var domain = "demo.servicebox.co.ls";
//for use in in-browser development
//var domain = "localhost:8000";
//for use with android emulator
//var domain = "10.0.2.2:8000";
var domain = "www.servicebox.co.ls";

var ServiceBoxApp = angular.module('starter', ['ionic', 'ngSanitize', 'ngCordova', 'angular-cache']);

ServiceBoxApp.config(['$stateProvider', '$ionicConfigProvider',
	function ($stateProvider, $ionicConfigProvider) {
		$ionicConfigProvider.tabs.position("bottom");
		$ionicConfigProvider.scrolling.jsScrolling(false);
		$stateProvider.
				state('home', {
					url: '/home',
					templateUrl: 'components/home/home-template.html',
					controller: 'HomeController'
				}).
				state('emergencyContacts', {
					url: '/emergencyContacts',
					templateUrl: 'components/emergencyContacts/emergencyContacts-template.html',
					controller: 'EmergencyContactsController'
				}).
				state('police', {
					url: '/police',
					templateUrl: 'components/police/police-template.html',
					controller: 'PoliceController'
				}).
				state('accommodation', {
					url: '/accommodation',
					templateUrl: 'components/accommodation/accommodation-template.html',
					controller: 'AccommodationController'
				}).
				state('mobileMoney', {
					url: '/mobileMoney',
					templateUrl: 'components/mobileMoney/mobileMoney-template.html',
					controller: 'MobileMoneyController'
				}).
				state('touristAttractions', {
					url: '/touristAttractions',
					templateUrl: 'components/touristAttractions/touristAttractions-template.html',
					controller: "TouristAttractionsController"
				}).
				state('atm-branchLocator', {
					url: '/atm-branchLocator',
					templateUrl: 'components/atm-branchLocator/atm-branchLocator-template.html',
					controller: 'atm-branchLocatorController'
				}).
				state('touristDetails', {
					url: '/touristDetails/:attractionId',
					templateUrl: 'components/touristAttractions/details/touristAttractions-details-template.html',
					controller: 'TouristDetailsController'
				}).
				state('tourist360', {
					url: '/tourist360/:attractionId',
					templateUrl: 'components/touristAttractions/360/touristAttractions-360-template.html',
					controller: 'Tourist360Controller'
				}).
				state('touristGallery',{
					url: '/touristGallery/:attractionId',
					templateUrl: 'components/touristAttractions/photoGallery/touristAttractions-gallery-template.html',
					controller: 'TouristGalleryController'
				}).
				state('directions',{
					url: '/directions/:lat/:lng',
					templateUrl: 'components/directions/directions-template.html',
					controller: 'DirectionsController'
				}).
				state('telephoneTaxis', {
					url: '/telephoneTaxis',
					templateUrl: 'components/telephone-taxis/telephone-taxis-template.html',
					controller: 'TelephoneTaxisController'
				});
}]);

ServiceBoxApp.controller('MainController', ['$scope', '$state', '$ionicSideMenuDelegate', '$ionicModal', function($scope, $state, $ionicSideMenuDelegate, $ionicModal) {

	/* Default state set */
	$state.go('home');

	/* Objects for each child scope created here in case data needs to be passed around*/
	$scope.police = {};

	$scope.accommodation = {};

	$scope.atmBranch = {};

	$scope.mobileMoney = {};

	$scope.touristAttractions = {};

	$scope.main = {};

	$scope.directions = {};

	$scope.emergency = {};

	$scope.telephoneTaxis = {};

	$scope.main.errorText = "There was a problem loading content; try again later.";

	$scope.main.showMenu = true;

	$scope.main.filteringByDistrict = false;

	/* Districts used throught the application so variables and logic declared here */
	$scope.main.districtsInUse = {
		1 : true,
		2 : true,
		3 : true,
		4 : true,
		5 : true,
		6 : true,
		7 : true,
		8 : true,
		9 : true,
		10 : true
	};

	$scope.main.allDistrictsInUse = true;

	$scope.$watch('main.allDistrictsInUse', function(newVal, oldVal) {
		if (!newVal) return;
		$scope.main.filteringByDistrict = false;
		for (var i = 1; i <= 10; i++) {
			if ($scope.main.allDistrictsInUse) {
				$scope.main.districtsInUse[i] = true;
			}
		}
	});

	$scope.updateToggle = function() {
		$scope.main.filteringByDistrict = true;
		var count = 0;
		for (var i = 1; i <= 10; i++) {
			if ($scope.main.districtsInUse[i]) count++;
		}
		if (count === 10) $scope.main.allDistrictsInUse = true;
		else $scope.main.allDistrictsInUse = false;
	};

	$scope.updateList = function() {
		if ($scope.main.allDistrictsInUse) return;
		$scope.main.filteringByDistrict = true;
		for (var i = 1; i <= 10; i++) {
			$scope.main.districtsInUse[i] = false;
		}
	};

	/* Legacy variables; change to alter sidebar*/
	$scope.expandedSidebar = true;

	$scope.sidebarWidth = 215;

	/* Stump function implemented by child controllers */
	$scope.main.districtsChanged = function() {
		return;
	};

	var setgmDefaults = function () {
		var c = new google.maps.LatLng(-29.5484737,28.2012221);
		$scope.main.defaultMapOptions = {
			center : c,
			zoom : 8,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};
	};
	/* Default options for maps use throught the application; center hardcoded as center of Lesotho */
	if (typeof google !== "undefined") {
		setgmDefaults();
	} else {
		$scope.main.error = "There is no internet connection; please connect to the internet then restart servicebox";
	}

	$scope.main.toKilometers = function(num) {
		return Math.round(num / 1000 * 100) / 100;
	};

	/* Loading modal logic */
	$ionicModal.fromTemplateUrl('components/loading-template.html', {
		scope: $scope,
		animation: 'slide-in-right'
	}).then(function(modal) {
		$scope.main.modal = modal;
	});

	$scope.main.showLoading = function() {
		$scope.main.modal.show();
	};

	$scope.main.doneLoading = function() {
		$scope.main.modal.hide();
	};

	/* Error modal logic */
	$ionicModal.fromTemplateUrl('components/error-template.html', {
		scope: $scope,
		animation: 'slide-in-right'
	}).then(function(modal) {
		$scope.main.errorModal = modal;
	});

	$scope.main.showError = function() {
		$scope.main.errorModal.show();
	};

	$scope.$on('$destroy', function() {
		$scope.main.modal.remove();
		$scope.main.errorModal.remove();
	});


	window.addEventListener("online", function(e) {
        //load google maps
        if (typeof google === "undefined") {
			var script = document.createElement("script");
			script.type = "text/javascript";
			script.id = "googleMaps";
			window.setgmDefaults = setgmDefaults;
			script.src = "http://maps.google.com/maps/api/js?v=3.25&libraries=geometry&key=AIzaSyBZZR8NnTgUE6q5x8qI3nGfPGYAT8xIwWI&callback=setgmDefaults";
			document.head.appendChild(script);
        }

    }, false);
}]);
