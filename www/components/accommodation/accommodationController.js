'use strict';

ServiceBoxApp.controller('AccommodationController', ['$scope', 'resultsCounterFactory', '$http', '$ionicModal', 'circleLocationFactory', '$timeout', '$ionicScrollDelegate', 'CacheFactory', '$q', '$ionicActionSheet', '$ionicSideMenuDelegate',
	function($scope, resultsCounterFactory, $http, $ionicModal, circleLocationFactory, $timeout, $ionicScrollDelegate, CacheFactory, $q, $ionicActionSheet, $ionicSideMenuDelegate) {
	var blankFunction = function() {};
	$scope.accommodation.loaded=false;
	$scope.accommodation.allCategoriesInUse = true;
	$scope.accommodation.accommodationType = [{icon : "b-b-marker.png", name : "B&B", show : true},
		{icon : "hotel-marker.png", name : "Hotel", show : true},
		{icon : "resort-marker.png", name : "Resort", show : true},
		{icon : "guest-house-marker.png", name : "Guest House", show : true},
		{icon : "lodge-marker.png", name : "Lodge" , show : true}
	];
	var getAccomodationType = function(accommodationplace) {
		for (var i = 0; i < 5; i++) {
			if (accommodationplace.icon === $scope.accommodation.accommodationType[i].icon) return i;
		}
		return -1;
	};
	$scope.accommodationArray = [];
	$scope.accommodation.filters = [function(accommodationplace){
		for (var i = 0; i < 5; i++) {
			if (!$scope.accommodation.accommodationType[getAccomodationType(accommodationplace)].show) return false;
		}
		return true;
	}, function(accommodationplace) {
		return $scope.main.districtsInUse[accommodationplace.district.data.id];
	}];

	$scope.accommodation.filters.push(function(accommodationplace) {
		return $scope.main.districtsInUse[accommodationplace.district.data.id];
	});

	/* Applies filters*/
	$scope.accommodation.applyFilters = function(accommodationplace){
		for (var i = 0; i < $scope.accommodation.filters.length; i++) {
			if (!$scope.accommodation.filters[i](accommodationplace)) {
				return false;
			}
		}
		return true;
	};

	/* Variables for location filter use*/
	$scope.accommodation.locationInUse = false;
	$scope.accommodation.circle = {};
	$scope.accommodation.distance = 5000;
	$scope.accommodation.updateCircle = blankFunction;
	$scope.accommodation.drawCircle = blankFunction;
	$scope.accommodation.removeCircle = blankFunction;

	var getLocationRadius = circleLocationFactory($scope.accommodation, function(newFilter){
		$scope.accommodation.drawCircle(); //implemented in map controller
		appendDistance();
		$scope.main.doneLoading();
	}, function() {
		$scope.error = "Location not available";
		$scope.accommodation.locationInUse = false;
		$scope.main.doneLoading();
		$scope.main.showError();
	});

	var appendDistance = function() {
		for (var i = 0; i < $scope.accommodationArray.length; i++) {
			if ($scope.accommodationArray[i].distance !== undefined) return;
			var p = new google.maps.LatLng($scope.accommodationArray[i].latitude, $scope.accommodationArray[i].longitude);
			$scope.accommodationArray[i].distance = Math.round((google.maps.geometry.spherical.computeDistanceBetween(p, $scope.accommodation.circle.center) | 0) / 1000 * 100)/100;
		}
		$scope.accommodationArray.sort(function(a, b){
			return a.distance - b.distance;
		});
	};

	/* Function to be run when location is toggled*/
	$scope.$watch('accommodation.locationInUse', function(flag, old){
		if (flag) {
			$scope.main.showLoading();
			getLocationRadius();
		} else {
			if ($scope.accommodation.filters.length > 1) {
				$scope.accommodation.filters.pop();
			}
			$scope.accommodation.removeCircle(); //implemented in map controller
		}
	});

	/* Function to be run whenever the distance slider is changed*/
	$scope.accommodation.distanceUpdated = function(distance){
		$timeout(function(){
			$scope.accommodation.circle.setRadius(parseInt(distance));
			$scope.accommodation.updateCircle();
		});
	};
	if (!CacheFactory.get('accommodationCache')) {
		CacheFactory('accommodationCache', {
			maxAge : 604800000,
			deleteOnExpire : 'aggressive',
			storageMode: 'localStorage',
			onExpire: function(key, value) {
				loadData().then(function(){
				}, function(){
					CacheFactory.get('accommodationCache').put(key, value);
				});
			}
		});
	}

	var accommodationCache = CacheFactory.get('accommodationCache');

	/* Logic to get accommodation data*/
	var getUrl = "http://" + domain + "/api/accommodation?district=1,2,3,4,5,6,7,8,9,10&type=1,2,3,4,5";

	var loadData = function() {
		var deferred = $q.defer();
		if (accommodationCache.get(getUrl)) {
			$scope.error = "";
			$scope.accommodationArray = accommodationCache.get(getUrl);
			$scope.accommodation.totalResults = resultsCounterFactory($scope.accommodationArray, $scope.accommodation.filters);
			$scope.accommodation.loaded = true;
			deferred.resolve();
		} else {
			$http.get(getUrl).then(function(response){
				$scope.error = "";
				$scope.accommodationArray = response.data.data;
				accommodationCache.put(getUrl, $scope.accommodationArray);
				$scope.accommodation.totalResults = resultsCounterFactory($scope.accommodationArray, $scope.accommodation.filters);
				$scope.accommodation.loaded=true;
				deferred.resolve();
			}, function(response){
				$scope.error = "Could not get accommodation data; no internet access or problem with database";
				deferred.reject();
			});
		}
		return deferred.promise;
	};

	loadData().then(function() {}, function(){
		$scope.main.showError();
	});

	/* Reset functions used by the map*/
	$scope.accommodation.tilesSelected = function() {
		$scope.accommodation.updateCircle = blankFunction;
		$scope.accommodation.drawCircle = blankFunction;
		$scope.accommodation.removeCircle = blankFunction;
		$scope.accommodation.updateMarkers = blankFunction;
	};

	$scope.accommodation.toggleFilter = function() {
		$ionicActionSheet.show({
			cssClass : "custom-sheet",
			buttons : [{text : "Around Me"}, {text : "Category"}, {text : "District"}],
			cancelText : "Cancel",
			titleText : "Choose Filters",
			buttonClicked : function(index) {
				switch (index) {
					case 1:
						$scope.accommodation.modal.show();
						break;
					case 0:
						$scope.accommodation.locationInUse = !$scope.accommodation.locationInUse;
						break;
					case 2:
						$ionicSideMenuDelegate.toggleRight();
						break;
					default:
						return false;
				}
				$ionicScrollDelegate.scrollTop();
				return true;
			}
		});
	};

	/* Category modal logic */
	$ionicModal.fromTemplateUrl('components/accommodation/accommodation-filter-modal-template.html', {
		scope: $scope,
		animation: 'slide-in-right'
	}).then(function(modal) {
		$scope.accommodation.modal = modal;
	});
	$scope.$watch('accommodation.allCategoriesInUse', function() {
		for (var i = 0; i < 5; i++) {
			if ($scope.accommodation.allCategoriesInUse) {
				$scope.accommodation.accommodationType[i].show = true;
			} 
		}
	});
	$scope.accommodation.updateToggle = function() {
		var count = 0;
		for (var i = 0; i < 5; i++) {
			if ($scope.accommodation.accommodationType[i].show) count++;
		}
		if (count === 5) $scope.accommodation.allCategoriesInUse = true;
		else $scope.accommodation.allCategoriesInUse = false;
	};

	$scope.accommodation.updateCList = function() {
		if ($scope.accommodation.allCategoriesInUse) return;
		for (var i = 0; i < 5; i++) {
			$scope.accommodation.accommodationType[i].show = false;
		}
	};
	// Cleanup the modal when we're done with it!
	$scope.$on('$destroy', function() {
		$scope.accommodation.modal.remove();
	});

	if (typeof window.ga !== "undefined") window.ga.trackView('mobile-accommodation');
}]);

ServiceBoxApp.controller('AccommodationMapController', ['$scope', 'populateMarkersFactory', 'updateMarkersFactory', function($scope, populateMarkersFactory, updateMarkersFactory){
	var markers = [];
	var createMarkerClickHandler = function(accommodationplace) {
		var content = '<div class="info-card"><div>'+accommodationplace.name+'</div><div>'+accommodationplace.address+'</div>';
		if (accommodationplace.hasPhone) content += '<div> <a href="tel:'+accommodationplace.phone.split(' ').join('')+'">'+accommodationplace.phone+'</a></div>';
		if (accommodationplace.hasCell) content += '<div> <a href="tel:'+accommodationplace.cell.split(' ').join('')+'">'+accommodationplace.cell+'</a></div>';
		if (accommodationplace.hasEmail) content += '<div> <a href="mailto:'+ accommodationplace.email +'">'+accommodationplace.email+'</a></div>';
		content += '<div><a href="#/directions/'+accommodationplace.latitude+'/'+accommodationplace.longitude+'">Get Directions</a></div></div>';
		return function($event) {
			var infoWindow = new google.maps.InfoWindow({
				content: content
			});
			infoWindow.open($scope.accommodation.map, this);
		};
	};

	/* Function for showing the circle, map is where the circle will be shown, the update flag is whether or not to update markers*/
	var showCircle = function(map, update) {
		$scope.accommodation.circle.setMap(map);
		$scope.accommodation.updateCircle = $scope.accommodation.updateMarkers;
		if (update) $scope.accommodation.updateMarkers();
	};

	/* Set the circle functions defined in the tile view*/
	var setCircleFunctions = function() {
		$scope.accommodation.drawCircle = function() {
			showCircle($scope.accommodation.map, true);
			$scope.accommodation.map.setCenter($scope.accommodation.circle.center);
		};

		$scope.accommodation.removeCircle = function() {
			showCircle(null, true);
			$scope.accommodation.map.setCenter($scope.main.defaultMapOptions.center);
		};
	};

	$scope.accommodation.loadMap = function() {
		$scope.main.showLoading();
		$scope.accommodation.map = new google.maps.Map(document.getElementById("mapa"), $scope.main.defaultMapOptions);
		var populateMarkersArray = populateMarkersFactory($scope.accommodation.map, markers, $scope.accommodationArray, function(accommodationplace) {
			return "img/accommodation/markers/" + accommodationplace.icon;
		}, createMarkerClickHandler);
		populateMarkersArray();
		$scope.accommodation.mapLoaded = true;
		$scope.accommodation.updateMarkers = updateMarkersFactory($scope.accommodationArray, $scope.accommodation.filters, markers, $scope.accommodation.map);
		$scope.accommodation.updateMarkers();
		$scope.main.districtsChanged = $scope.accommodation.updateMarkers;
		$scope.main.doneLoading();
	};
	$scope.accommodation.loadMap();
	if ($scope.accommodation.locationInUse) showCircle($scope.accommodation.map, false);
	setCircleFunctions();

	$scope.accommodation.mapSelected = function() {
		$scope.main.showLoading();
		$scope.accommodation.updateMarkers = updateMarkersFactory($scope.accommodationArray, $scope.accommodation.filters, markers, $scope.accommodation.map);
		$scope.accommodation.updateMarkers();
		$scope.main.districtsChanged = $scope.accommodation.updateMarkers;
		setTimeout(function(){
			google.maps.event.trigger($scope.accommodation.map, 'resize');
		}, 100);
		if ($scope.accommodation.locationInUse) {
			showCircle($scope.accommodation.map, false);
		} else if ($scope.accommodation.circle.setMap !== undefined) {
			showCircle(null, false);
		}
		setCircleFunctions();
		$scope.main.doneLoading();
	};
}]);