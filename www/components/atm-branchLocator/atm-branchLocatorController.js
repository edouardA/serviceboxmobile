'use strict';
ServiceBoxApp.controller('atm-branchLocatorController', ['$scope', 'resultsCounterFactory', '$http', '$q','$ionicSideMenuDelegate', '$rootScope', '$timeout', 'circleLocationFactory', '$ionicScrollDelegate', 'CacheFactory', '$ionicActionSheet',
	function($scope, resultsCounterFactory, $http, $q, $ionicSideMenuDelegate, $rootScope, $timeout, circleLocationFactory, $ionicScrollDelegate, CacheFactory, $ionicActionSheet) {
		var blankFunction = function() {};

		$scope.atmArray = [];
		$scope.branchArray = [];
		$scope.atmBranch.bankChosen = false;
		$scope.atmBranch.locationInUse = false;
		$scope.atmBranch.circle = {};
		$scope.atmBranch.distance = 5000;
		$scope.atmBranch.updateCircle = blankFunction;
		$scope.atmBranch.drawCircle = blankFunction;
		$scope.atmBranch.removeCircle = blankFunction;
		$scope.atmBranch.loaded = false;
		$scope.atmBranch.showBankPicker = false;


		/* Filters for atmBranchLocator*/
		$scope.atmBranch.filters = [function(atmBranch){
			return $scope.main.districtsInUse[atmBranch.district.data.id];
		}];

		var getLocationRadius = circleLocationFactory($scope.atmBranch, function(newFilter){
			$scope.atmBranch.drawCircle(); //implemented in map controller
			appendDistance();
			$scope.main.doneLoading();
		}, function() {
			$scope.error = "Location not available";
			$scope.atmBranch.locationInUse = false;
			$scope.main.doneLoading();
			$scope.main.showError();
		});

		var appendDistance = function() {
			for (var i = 0; i < $scope.atmArray.length; i++) {
				var p = new google.maps.LatLng($scope.atmArray[i].latitude, $scope.atmArray[i].longitude);
				$scope.atmArray[i].distance = Math.round((google.maps.geometry.spherical.computeDistanceBetween(p, $scope.atmBranch.circle.center) | 0) / 1000 * 100) / 100;
			}
			$scope.atmArray.sort(function(a, b){
				return a.distance - b.distance;
			});
			for (i = 0; i < $scope.branchArray.length; i++) {
				var p = new google.maps.LatLng($scope.branchArray[i].latitude, $scope.branchArray[i].longitude);
				$scope.branchArray[i].distance = Math.round((google.maps.geometry.spherical.computeDistanceBetween(p, $scope.atmBranch.circle.center) | 0)/ 1000 * 100) / 100;
			}
			$scope.branchArray.sort(function(a, b){
				return a.distance - b.distance;
			});
			createCombinedArray();
			$scope.combinedAtmBranchArray.sort(function(a, b){
				return a.distance - b.distance;
			});
		};

		/* Function to be run when location is toggled*/
		$scope.$watch('atmBranch.locationInUse', function(flag){
			if (flag) {
				$scope.main.showLoading();
				getLocationRadius();
			} else {
				if ($scope.atmBranch.filters.length > 1) {
					$scope.atmBranch.filters.pop();
				}
				$scope.atmBranch.removeCircle(); //implemented in map controller
			}
		});

		/* Function to be run whenever the distance slider is changed*/
		$scope.atmBranch.distanceUpdated = function(distance){
			$timeout(function(){
				$scope.atmBranch.circle.setRadius(parseInt(distance));
				$scope.atmBranch.updateCircle();
			});
		};

		/* Applies filters*/
		$scope.atmBranch.applyFilters = function(atmBranch){
			for (var i = 0; i < $scope.atmBranch.filters.length; i++) {
				if (!$scope.atmBranch.filters[i](atmBranch)) {
					return false;
				}
			}
			return true;
		};

		if (!CacheFactory.get('atmBranchCache')) {
			CacheFactory('atmBranchCache', {
				maxAge : 604800000,
				deleteOnExpire : 'aggressive',
				storageMode: 'localStorage',
				onExpire: function(key, value) {
					loadData(key).then(function(){
					}, function(){
						CacheFactory.get('atmBranchCache').put(key, value);
					});
				}
			});
		}

		var atmBranchCache = CacheFactory.get('atmBranchCache');

		var createCombinedArray = function() {
			$scope.combinedAtmBranchArray = $scope.atmArray.concat($scope.branchArray);
			$scope.combinedAtmBranchArray.sort(function(a, b){
				return a.location - b.location;
			});
			console.log($scope.combinedAtmBranchArray);
		};

		var loadData = function (id) {
			var deferred = $q.defer();
			$scope.atmBranch.loaded = false;
			$scope.atmBranch.bankChosen = true;
			var getAtmUrl = "http://" + domain + "/api/banks/atms?bank="+id+"&district=1,2,3,4,5,6,7,8,9,10";
			var getBranchUrl = "http://" + domain + "/api/banks/branches?bank="+id+"&district=1,2,3,4,5,6,7,8,9,10";
			if (atmBranchCache.get(id)) {
				$scope.error = "";
				$scope.atmArray = atmBranchCache.get(id)[0];
				$scope.atmBranch.totalAtms = resultsCounterFactory($scope.atmArray, $scope.atmBranch.filters);
				$scope.branchArray = atmBranchCache.get(id)[1];
				$scope.atmBranch.totalBranches = resultsCounterFactory($scope.branchArray, $scope.atmBranch.filters);
				createCombinedArray();
				$rootScope.$broadcast('arrayLoaded');
				$scope.atmBranch.loaded = true;
				deferred.resolve();
			} else {
				$q.all([$http.get(getAtmUrl), $http.get(getBranchUrl)]).then(function(results) {
					$scope.error = "";
					$scope.atmArray = results[0].data.data;
					$scope.atmBranch.totalAtms = resultsCounterFactory($scope.atmArray, $scope.atmBranch.filters);
					$scope.branchArray = results[1].data.data;
					$scope.atmBranch.totalBranches = resultsCounterFactory($scope.branchArray, $scope.atmBranch.filters);
					createCombinedArray();
					$rootScope.$broadcast('arrayLoaded');
					$scope.atmBranch.loaded = true;
					atmBranchCache.put(id, [$scope.atmArray, $scope.branchArray]);
					deferred.resolve();
				}, function(results){
					$scope.error = "Could not get ATM and/or Branch list; no internet access or problem with the database";
					$scope.atmBranch.loaded = true;
					deferred.reject();
				});
			}
			return deferred.promise;
		};

		/* Function to run when a bank is selected from the dropdown */
		$scope.atmBranch.bankSelected = function(id) {
			loadData(id).then(function(){
			}, function(){
				$scope.main.showError();
			});
		};

		/* Filter button and select menu logic */
		var showFilterTiles = function() {
			$ionicActionSheet.show({
				cssClass : "custom-sheet",
				buttons : [{text : "Around Me"}, {text : "District"}, {text : "Switch Bank"}],
				titleText : "Choose Filters",
				cancelText : 'Cancel',
				buttonClicked : function(index) {
					switch (index) {
						case 1:
							$ionicSideMenuDelegate.toggleRight();
							break;
						case 0:
							$scope.atmBranch.locationInUse = !$scope.atmBranch.locationInUse;
							break;
						case 2:
							$scope.atmBranch.showBankPicker = !$scope.atmBranch.showBankPicker;
							break;
						default:
							return false;
					}
					$ionicScrollDelegate.scrollTop();
					return true;
				}
			});
		};

		$scope.atmBranch.toggleFilter = showFilterTiles;

		/* Logic to reset function when tilesSelected */
		$scope.atmBranch.tilesSelected = function() {
			$scope.atmBranch.toggleFilter = showFilterTiles;
			$scope.atmBranch.updateMarkers = function(){
			};
		};

		if (typeof window.ga !== "undefined") window.ga.trackView('mobile-atmBranch');
}]);


ServiceBoxApp.controller('atm-branchLocatorMapController', ['$scope', 'populateMarkersFactory', 'updateMarkersFactory', '$ionicSideMenuDelegate', '$ionicActionSheet', function($scope, populateMarkersFactory, updateMarkersFactory, $ionicSideMenuDelegate, $ionicActionSheet){
	/* Marker Arrays */
	var atmMarkers = [];
	var branchMarkers = [];

	/* Click handlers for atms and branches*/
	var createAtmMarkerClickHandler = function(atm){
		return function($event) {
			var content = '<div class="info-card"><div><a href="tel:'+atm.phone.split(' ').join('')+'">'+atm.phone+'</a></div><div>'+atm.location+'</div><div>'+atm.district.data.name+'</div>';
			content += '<div><a href="#/directions/'+atm.latitude+'/'+atm.longitude+'">Get Directions</a></div></div>';
			var infoWindow = new google.maps.InfoWindow({
				content: content
			});
			infoWindow.open($scope.atmBranch.map, this);
		};
	};

	var createBranchMarkerClickHandler = function(branch) {
		return function($event) {
			var content = '<div class="info-card"> <div>' +branch.district.data.name+ '</div><div><b>' +branch.name+ '</b></div><div>Branch</div><div>' +branch.location+ '</div><div>Branch Details</div><div>Code: ' +branch.code+'<br>Phone: <a href="tel:' + branch.phone.split(' ').join('') + '">' +branch.phone+'</a></div><div>Hours of Operation</div><div><b> Mon-Fri: </b> '+branch.week_op_hrs+'<br><b> Sat: </b> '+branch.sat_op_hrs+'<br><b> Sun: </b> '+ branch.sun_op_hrs+'</div></div>';
			var infoWindow = new google.maps.InfoWindow({
				content: content
			});
			infoWindow.open($scope.atmBranch.map, this);
		};
	};

	/* ATM/branch toggle logic */
	var hideAtmMarkers = function() {
		for (var i = 0; i < atmMarkers.length; i++){
			atmMarkers[i].setMap(null);
		}
	};

	var showAtmMarkers = function() {
		for (var i = 0; i < atmMarkers.length; i++) {
			atmMarkers[i].setMap($scope.atmBranch.map);
		}
	};

	/* Function for showing the circle, map is where the circle will be shown, the update flag is whether or not to update markers*/
	var showCircle = function(map, update) {
		$scope.atmBranch.circle.setMap(map);
		$scope.atmBranch.updateCircle = updateMarkersMap;
		if (update) updateMarkersMap();
	};

	/* Set the circle functions defined in the tile view*/
	var setCircleFunctions = function() {
		$scope.atmBranch.drawCircle = function() {
			showCircle($scope.atmBranch.map, true);
			$scope.atmBranch.map.setCenter($scope.atmBranch.circle.center);
		};

		$scope.atmBranch.removeCircle = function() {
			showCircle(null, true);
			$scope.atmBranch.map.setCenter($scope.main.defaultMapOptions.center);
		};
	};

	/* Function to tun whenever markers need updating*/
	var updateMarkersMap = function() {
		updateMarkersFactory($scope.atmArray, $scope.atmBranch.filters, atmMarkers, $scope.atmBranch.map)();
		updateMarkersFactory($scope.branchArray, $scope.atmBranch.filters, branchMarkers, $scope.atmBranch.map)();
	};

	/* Function to load the map*/
	$scope.atmBranch.loadMap = function() {
		$scope.main.showLoading();
		$scope.atmBranch.map = new google.maps.Map(document.getElementById("mapab"), $scope.main.defaultMapOptions);
		var populateAtmMarkers = populateMarkersFactory($scope.atmBranch.map, atmMarkers, $scope.atmArray, function(atm) {
			return "img/banks/std-atm.png";
		}, createAtmMarkerClickHandler);
		var populateBranchMarkers = populateMarkersFactory($scope.atmBranch.map, branchMarkers, $scope.branchArray, function(branch){
			return "img/banks/std-branch.png";
		}, createBranchMarkerClickHandler);
		populateAtmMarkers();
		populateBranchMarkers();
		$scope.main.districtsChanged = updateMarkersMap;
		updateMarkersMap();
		if ($scope.atmBranch.locationInUse) showCircle($scope.atmBranch.map, false);
		setCircleFunctions();
		$scope.main.doneLoading();
	};

	/* If a bank is already chosen load the map*/
	if ($scope.atmBranch.bankChosen) $scope.atmBranch.loadMap();

	/* When the array is loaded (which happens whenever a new bank is chosen), reload the map */
	$scope.$on('arrayLoaded', function(event){
		$scope.atmBranch.loadMap();
	});

	var showFilterMap = function() {
		$ionicActionSheet.show({
			cssClass: "custom-sheet",
			buttons : [{text : "Around Me"}, {text: "ATM"}, {text : "Branches"}, {text : "District"}, {text : "Switch Bank"}],
			titleText : "Choose Filters",
			cancelText : 'Cancel',
			buttonClicked : function(index) {
				switch (index) {
					case 3:
						$ionicSideMenuDelegate.toggleRight();
						break;
					case 0:
						$scope.atmBranch.locationInUse = !$scope.atmBranch.locationInUse;
						break;
					case 4:
						$scope.atmBranch.showBankPicker = !$scope.atmBranch.showBankPicker;
						break;
					case 2:
						hideAtmMarkers();
						break;
					case 1:
						showAtmMarkers();
						break;
					default:
						return false;
				}
				return true;
			}
		});
	};

	$scope.atmBranch.toggleFilter = showFilterMap;

	/* Logic to run whenever the map is selected */
	$scope.atmBranch.mapSelected = function() {
		$scope.main.showLoading();
		$scope.atmBranch.toggleFilter = showFilterMap;
		$scope.main.districtsChanged = updateMarkersMap;
		updateMarkersMap();
		setTimeout(function(){
			google.maps.event.trigger($scope.atmBranch.map, 'resize');
			$scope.atmBranch.map.setCenter($scope.main.defaultMapOptions.center);
		}, 100);
		if ($scope.atmBranch.locationInUse) {
			showCircle($scope.atmBranch.map, false);
		} else if ($scope.atmBranch.circle.setMap !== undefined){
			showCircle(null, false);
		}
		setCircleFunctions();
		$scope.main.doneLoading();
	};
}]);