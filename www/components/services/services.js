'use strict';

/* Takes in an array of values and of filtering functions; 
returns a function which will count the elements in the array which satisfy all the filters*/
ServiceBoxApp.factory('resultsCounterFactory', function(){
	return function(arrayOfValues, arrayOfCheckFunctions) {
		return function() {
			var count = 0;
			for (var i = 0; i < arrayOfValues.length; i++) {
				var failedCheck = false;
				if (arrayOfCheckFunctions !== null && arrayOfCheckFunctions !== undefined) {
					for (var j = 0; j < arrayOfCheckFunctions.length; j++) {
						if(!arrayOfCheckFunctions[j](arrayOfValues[i])) {
							failedCheck = true;
							break;
						}
					}
				}
				if (!failedCheck) count++;
			}
			return count;
		};
	};
});

/* Takes in a map, an empty array of markers, an array of values for the markers, a function to get an icon for each marker and a function to create a click handler;
returns a function which will populate the markers array and put the markers on the map*/
ServiceBoxApp.factory('populateMarkersFactory', function(){
	return function(map, markers, arrayOfValues, getIcon, createMarkerClickHandler) {
		return function () {
			for (var i = 0; i < arrayOfValues.length; i++) {
				var value = arrayOfValues[i];
				var p = new google.maps.LatLng(value.latitude, value.longitude);
				if (isNaN(p.lat()) || isNaN(p.lng())) p = new google.maps.LatLng(value.lat, value.lng); //In case object stores coordinates differently
				var img = getIcon(value);
				var marker = new google.maps.Marker({
					position : p,
					map : map,
					title : value.name,
					icon : img
				});
				marker.addListener('click', createMarkerClickHandler(value));
				markers.push(marker);
			}
		};
	};
});

/* Takes in an array of values, an array of functions to filter the array, an array of markers whose indexes match up with the array of values and the map for the markers;
returns a function which when called will update the markers array (and the map) based on the filter functions*/
ServiceBoxApp.factory('updateMarkersFactory', function(){
	return function(arrayOfValues, arrayOfCheckFunctions, markers, map) {
		return function() {
			for (var i = 0; i < arrayOfValues.length; i++) {
				var failedCheck = false;
				var value = arrayOfValues[i];
				if (arrayOfCheckFunctions !== null && arrayOfCheckFunctions !== undefined) {
					for (var j = 0; j < arrayOfCheckFunctions.length; j++) {
						if (!arrayOfCheckFunctions[j](value)) {
							failedCheck = true;
							break;
						}
					}
				}
				if (failedCheck) markers[i].setMap(null);
				else markers[i].setMap(map);
			}
		};
	};
});

/* Takes in an object containing at least a circle, a default "distance" which sepcifies the radius of the circe 
and a filters array and a callback to be run once the standard behavior is accompished (the callback takes in the new filter in case you need it)
returns a function which will get the users location ancd create a google maps circle around the location as well as appending a filter to the filters 
array which checks if the entry is within the circle */
ServiceBoxApp.factory('circleLocationFactory', ['$ionicPlatform', '$cordovaGeolocation', function($ionicPlatform, $cordovaGeolocation) {
	return function(object, callback, errorCallback) {
		return function() {
			$ionicPlatform.ready(function(){
				$cordovaGeolocation.getCurrentPosition({timeout : 5000}).then(function(position){
					var lat = position.coords.latitude;
					var lng = position.coords.longitude;
					object.circle = new google.maps.Circle({
						strokeColor: '#FF0000',
						strokeOpacity: 0.8,
						strokeWeight: 2,
						fillColor: '#FF0000',
						fillOpacity: 0.35,
						radius : parseInt(object.distance),
						center: {lat : lat, lng : lng}
					});
					var newFilter = function(place){
						var p = new google.maps.LatLng(place.latitude, place.longitude);
						if (isNaN(p.lat()) || isNaN(p.lng())) p = new google.maps.LatLng(place.lat, place.lng); //In case object stores coordinates differently
						return google.maps.geometry.spherical.computeDistanceBetween(p, object.circle.center) <= object.circle.radius;
					};
					object.filters.push(newFilter);
					callback(newFilter);
				}, function(error){
					if (errorCallback) errorCallback();
				});
			});
		};
	};
}]);