'use strict';
ServiceBoxApp.controller('EmergencyContactsController', ['$scope', '$http', '$ionicSideMenuDelegate', 'CacheFactory', '$q', '$ionicActionSheet', '$ionicScrollDelegate',
 function($scope, $http, $ionicSideMenuDelegate, CacheFactory, $q, $ionicActionSheet, $ionicScrollDelegate) {

	$scope.emergency.loaded = false;

	$scope.emergency.toggleFilter = function() {
		$ionicActionSheet.show({
			cssClass : "custom-sheet",
			buttons : [{text : "District"}],
			titleText : 'Choose Filters',
			cancelText : 'Cancel',
			buttonClicked : function(index) {
				switch (index) {
					case 0:
						$ionicScrollDelegate.scrollTop();
						$ionicSideMenuDelegate.toggleRight();
						break;
					default:
						return false;
				}
				return true;
			}
		});
	};

	var locationToDistrict = function (location) {
		switch (location) {
			case "Mapoteng":
			case "Teyateyaneng":
			case "Berea":
				return 1;
			case "Botha-Bothe":
				return 2;
			case "Maputsoe":
			case "Leribe":
				return 3;
			case "Mafeteng":
				return 4;
			case "Roma":
			case "Maseru":
				return 5;
			case "Mahale's Hoek":
			case "Mohale's Neck":
				return 6;
			case "Mokhotlong":
				return 7;
			case "Qacha's Nek":
				return 8;
			case "Quthing":
				return 9;
			case "Morija":
			case "Thaba-Tseka":
				return 10;
			default:
				return 0;
		}
	};

	$scope.emergency.filterFunc = function(emergencyContact){
		if (emergencyContact.location === "All Districts") {
			return true;
		} 
		return $scope.main.districtsInUse[locationToDistrict(emergencyContact.location)];
	};

	$scope.emergencyCategory = {
		1 : "Fire",
		2 : "Ambulance & Hospital",
		3 : "Police",
		4 : "Domestic Violence Hotline",
		5 : "Water & Sewage",
		6 : "Electricity"
	};

	$scope.emergencyCategoryIcon = {
		1 : "ion-flame",
		2 : "ion-medkit",
		3 : "ion-android-star-outline",
		4 : "ion-ios-pulse",
		5 : "ion-waterdrop",
		6 : "ion-outlet"
	};

	$scope.emergencyContactsArray = [];

	var getUrl = "http://" + domain + "/api/contacts/emergency";

	if (!CacheFactory.get('emergencyCache')) {
		CacheFactory('emergencyCache', {
			maxAge: 604800000,
			deleteOnExpire: 'aggressive',
			storageMode: 'localStorage',
			onExpire : function(key, value) {
				loadData().then(function(){
				}, function(){
					CacheFactory.get('emergencyCache').put(key, value);
				});
			}
		});
	}

	var emergencyCache = CacheFactory.get('emergencyCache');

	var loadData = function() {
		var deferred = $q.defer();
		if (emergencyCache.get(getUrl)) {
			$scope.error = "";
			$scope.emergencyContactsArray = emergencyCache.get(getUrl);
			$scope.emergency.loaded = true;
			deferred.resolve();
		} else {
			$http.get(getUrl).then(function(response){
				$scope.error = "";
				$scope.emergencyContactsArray = response.data.data;
				emergencyCache.put(getUrl, $scope.emergencyContactsArray);
				$scope.emergency.loaded = true;
				deferred.resolve();
			}, function(response){
				$scope.error = "Could not get emergency contacts; no internet access or problem with database: " + response.status + " " + response.statusText;
				deferred.reject();
			});
		}
		return deferred.promise;
	};

	loadData().then(function(){
	}, function(){
		$scope.main.showError();
	});

	if (typeof window.ga !== "undefined") window.ga.trackView('mobile-emergencyContacts');
}]);